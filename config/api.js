var apiai = require('apiai');
var app = apiai("b7b14fa926454d0fbd68766ce4ee42d8");
// Function which returns speech from api.ai
var getRes = function (query) {
    var request = app.textRequest(query, {
        sessionId: 'Test1'
    });
    const responseFromAPI = new Promise(
        function (resolve, reject) {
            request.on('error', function (error) {
                reject(error);
            });
            request.on('response', function (response) {
                resolve(response.result.fulfillment.speech);
            });
        });
    request.end();
    return responseFromAPI;
};
// test the command :
//getRes('hello').then(function(res){console.log(res)});
module.exports = { getRes }